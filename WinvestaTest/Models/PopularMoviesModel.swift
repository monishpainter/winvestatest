//
//  PopularMoviesModel.swift
//  WinvestaTest
//
//  Created by Monish Painter on 17/07/21.
//  Copyright © 2021 Monish Painter. All rights reserved.
//

import Foundation

final class PopularMoviesModel {
    
    private let paginator = MoviesPaginator()
    private let api: MoviesAPI
    
    init(api: MoviesAPI) {
        self.api = api
    }
    
}


// MARK: - PaginatedMoviesListModel

extension PopularMoviesModel: PaginatedMoviesListModel {
    
    var lastPage: Int {
        return paginator.lastPage
    }
    
    var state: Observable<PaginatedMoviesListState> {
        return paginator.state
    }
    
    func load(page: Int)  {
        api.discover(page: page, success: {[paginator] response in
            paginator.add(
                movies: response.results.map(MoviesAPI.toMovie), toPage: page
            )
            }, failure: { [paginator] error in
                paginator.handleAPIError(error)
        })
    }
}
