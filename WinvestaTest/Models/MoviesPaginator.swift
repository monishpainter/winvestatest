//
//  MoviesPaginator.swift
//  WinvestaTest
//
//  Created by Monish Painter on 17/07/21.
//  Copyright © 2021 Monish Painter. All rights reserved.
//

import Foundation

enum PaginatedMoviesListState {
    case loading
    case movies([RObjMovie])
    case fatalError(String)
    case pageError(String)
}



class MoviesPaginator {
    
    var state = Observable(PaginatedMoviesListState.movies([]))
    
    var lastPage: Int {
        return lock.sync { return self._lastPage }
    }
    
    private var movies: [RObjMovie] = [] {
        didSet {
            state.value = .movies(movies)
        }
    }

    
    private let lock = DispatchQueue(label: "com.PaginatedMoviesListModel.dispatchQueue")
    private var _lastPage: Int = 0
    
    func add(movies moviesToAdd: [RObjMovie], toPage page: Int) {
        DispatchQueue.main.async{
            DBManager.sharedInstance.addAllObjects(list: moviesToAdd, update: .modified)
        }
        lock.async { [weak self] in
            guard let self = self else { return }
            self.movies.append(contentsOf:moviesToAdd)
            self._lastPage = max(self._lastPage, page)
        }
    }
    
    
    func clear() {
        lock.async { [weak self] in
            guard let self = self else { return }
            self.movies.removeAll()
            self._lastPage = 0
        }
        
    }
    
    func localDataDisplay(){
        DispatchQueue.main.async{
            self.movies = DBManager.sharedInstance.fetchObjectsInArray(RObjMovie.self)
            self._lastPage = 0
        }
    }
    
    func handleAPIError(_ error: MoviesAPI.APIError) {
        switch error {
        case .parsingError(_), .emptyResult:
            state.value = .pageError("There are no more pages")
            
        case .networkError(let error):
            state.value = .fatalError(error.localizedDescription)
            
        case .noInternet:
            localDataDisplay()
            break
        }
        
    }
}


protocol PaginatedMoviesListModel {
    func load(page: Int)
    var state: Observable<PaginatedMoviesListState> { get }
    var lastPage: Int {get}
}
