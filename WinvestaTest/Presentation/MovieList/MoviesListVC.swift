//
//  MoviesListVC.swift
//  WinvestaTest
//
//  Created by Monish Painter on 17/07/21.
//  Copyright © 2021 Monish Painter. All rights reserved.
//

import UIKit


class MoviesListVC: UIViewController {
    
    @IBOutlet weak var emptyDatasetLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: MoviesListViewModel!
    private var cancelSubscription: CancelSubscription?
    private let api = MoviesAPI(session: URLSession.shared)
    var popularMoviesModel : PopularMoviesModel?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        popularMoviesModel = PopularMoviesModel(api: api)
        viewModel =  MoviesListViewModel(model: popularMoviesModel!)

        tableView.register(MovieCell.nib, forCellReuseIdentifier: MovieCell.identifier)
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 198
        
        cancelSubscription = viewModel.state.subscribe(on: .main) { [weak self] state in
            self?.handle(state)
        }
        
        
        viewModel.loadNextPage()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableView.reloadData()
    }
    
    
    func handle(_ state: MoviesListViewModel.State) {
        switch state {
            
        case .loading, .empty:
            tableView.isHidden = true
            emptyDatasetLabel.isHidden = false
            
        case .error(let message):
            tableView.isHidden = false
            emptyDatasetLabel.isHidden = true
            
            let alert = UIAlertController(
                title: "ERROR",
                message: message,
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            present(alert, animated: true)
            
        case .movies:
            tableView.isHidden = false
            emptyDatasetLabel.isHidden = true
            tableView.reloadData()
        }
    }
    
    deinit {
        cancelSubscription?()
    }
}

extension MoviesListVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.moviesCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let movie = viewModel.movie(at: indexPath.row)!
        let cell = tableView.dequeueReusableCell(withIdentifier: MovieCell.identifier) as! MovieCell
        cell.configure(with: movie)
        return cell
    }
    
 }

extension MoviesListVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == viewModel.moviesCount - 7 {
            viewModel.loadNextPage()
        }
        
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            tableView.tableFooterView = spinner
            tableView.tableFooterView?.isHidden = false
        }

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        viewModel.selectMovie(at: indexPath.row)
    }
}


