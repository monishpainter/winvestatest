//
//  MovieCell.swift
//  WinvestaTest
//
//  Created by Monish Painter on 17/07/21.
//  Copyright © 2021 Monish Painter. All rights reserved.
//

import UIKit
import SDWebImage

class MovieCell: UITableViewCell {
    
    @IBOutlet var posterImageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var overviewLabel: UILabel!
    
    static let identifier: String = "MovieCell"
    
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: Bundle(for: self))
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        posterImageView.image = nil
        nameLabel.text = nil
        overviewLabel.text = nil
    }
    
    func configure(with movie: RObjMovie) {
        nameLabel.text = movie.name
        overviewLabel.text = movie.overview
        if let strPoster = movie.poster{
            if let imageURL = URL(string: strPoster) {
                posterImageView?.sd_setImage(with: imageURL, placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    
                })
            }
        }
    }
}
