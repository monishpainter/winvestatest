//
//  ResMovies.swift
//  WinvestaTest
//
//  Created by Monish Painter on 17/07/21.
//  Copyright © 2021 Monish Painter. All rights reserved.
//

import Foundation
import RealmSwift

extension MoviesAPI {

    /// A movie list JSON response
    struct MoviesListResponse: Codable {
        let page: Int
        let totalResults: Int
        let totalPages: Int
        let results: [MovieResponse]
        
        enum CodingKeys: String, CodingKey {
            case page = "page"
            case totalResults = "total_results"
            case totalPages = "total_pages"
            case results = "results"
        }
    }
    
    /// A single movie block
    struct MovieResponse: Codable {
        let id: Int
        let title: String
        let posterPath: String?
        let overview: String?
        
        enum CodingKeys: String, CodingKey {
            case id = "id"
            case title = "title"
            case posterPath = "poster_path"
            case overview = "overview"
        }
    }
    
}

struct Movie {
    let id: Int
    let poster: URL?
    let name: String
    let overview: String
}

class RObjMovie : Object {
    
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String?
    @objc dynamic var overview: String?
    @objc dynamic var poster: String?
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

