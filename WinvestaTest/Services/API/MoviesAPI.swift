//
//  MoviesAPI.swift
//  WinvestaTest
//
//  Created by Monish Painter on 17/07/21.
//  Copyright © 2021 Monish Painter. All rights reserved.
//

import Foundation

final class MoviesAPI {
    
    enum APIError: Error {
        case parsingError(String)
        case networkError(Error)
        case emptyResult
        case noInternet
    }
    
    static let imageBaseURL = URL(string: "http://image.tmdb.org/t/p/")!
    private let baseURL = "https://api.themoviedb.org/3/"
    private let apiKEY = "2696829a81b1b5827d515ff121700838"
    
    private let session: URLSessionProtocol
    
    init(session: URLSessionProtocol) {
        self.session = session
    }
    
    
}


// MARK: - Interface -

extension MoviesAPI {
    
    /// Discover popular movies request
    ///
    /// - Parameters:
    ///   - page: page
    ///   - success: callback
    ///   - failure: callback
    func discover(
        page: Int,
        success: @escaping (MoviesListResponse) -> Void,
        failure: @escaping (APIError) -> Void)
    {
        
        // hardcoded for simplicity, in real project it would be request builder
        let endpoint = baseURL + "discover/movie?sort_by=popularity.desc&api_key=\(apiKEY)&page=\(page)"
        let url = URL(string: endpoint)!
        let discoverRequest = URLRequest(url: url)
        request(discoverRequest, success: success, failure: failure)
    }
    
}


// MARK: - Networking -
// this must be a separated class in a real project
extension MoviesAPI {
    
    private func request(
        _ request: URLRequest,
        success: @escaping (MoviesListResponse) -> Void,
        failure: @escaping (APIError) -> Void)
    {
        if Reachability.isConnectedToNetwork(){
            let task = session.dataTask(with: request) { (data, response, error) in
                if let error = error {
                    failure(.networkError(error))
                    return
                }
                
                guard let data = data else {
                    failure(.emptyResult)
                    return
                }
                
                do {
                    let value = try JSONDecoder().decode(
                        MoviesListResponse.self,
                        from: data
                    )
                    success(value)
                } catch {
                    print(error)
                    failure(.parsingError(error.localizedDescription))
                }
            }
            task.resume()
        }else{
            failure(.noInternet)
        }
    }
}

extension MoviesAPI {
    // MovieResponse to Movie converter
    static func toMovie(_ data: MoviesAPI.MovieResponse) -> RObjMovie {
        
        let poster: URL? = {
            guard let posterPath = data.posterPath else {return nil}
            return imageBaseURL
                .appendingPathComponent("w780")
                .appendingPathComponent(posterPath)
        }()
        
        let obj = RObjMovie()
        obj.id = data.id
        obj.name = data.title
        if let url = poster{
            obj.poster = "\(url)"
        }
        obj.overview = data.overview ?? ""
        
        return obj
    }
}
