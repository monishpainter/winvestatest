//
//  RealmHelper.swift
//  WinvestaTest
//
//  Created by Monish Painter on 19/07/21.
//  Copyright © 2021 Monish Painter. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

class DBManager {
    
    var database:Realm
    
    static let sharedInstance:DBManager = {
        let instance = DBManager ()
        return instance
    } ()
    
    private init() {
        print("\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])")
        let config = Realm.Configuration(
            // Set the new schema version. This must be greater than the previously used
            // version (if you've never set a schema version before, the version is 0).
            schemaVersion: 0,
            migrationBlock: { migration, oldSchemaVersion in
                if oldSchemaVersion < 0 {
                    
                }
        })
        Realm.Configuration.defaultConfiguration = config
        database = try! Realm()
    }
    
    
    private func safeTransaction(withBlock block: @escaping ()
        -> Void) {
        if !DBManager.sharedInstance.database.isInWriteTransaction {
            DBManager.sharedInstance.database.beginWrite()
        }
        block()
        if DBManager.sharedInstance.database.isInWriteTransaction {
            do {
                try DBManager.sharedInstance.database.commitWrite()
            }
            catch {
                DBManager.sharedInstance.database.cancelWrite()
            }
        }
    }
    
    func fetchObjects<T: Object>(_ type: T.Type, _ predicate: NSPredicate? = nil, _ order: [SortDescriptor]? = nil) -> Results<T>? {
        var results = DBManager.sharedInstance.database.objects(type)
        if predicate != nil {
            results = results.filter(predicate!)
        }
        if order != nil {
            results = results.sorted(by: order!)
        }
        return results
    }
    
    func fetchObjectsInArray<T: Object>(_ type: T.Type, _ predicate: NSPredicate? = nil, _ order: [SortDescriptor]? = nil) -> [T] {
        var results = DBManager.sharedInstance.database.objects(type)
        if predicate != nil {
            results = results.filter(predicate!)
        }
        if order != nil {
            results = results.sorted(by: order!)
        }
        return results.toArray(ofType: T.self)
    }
    
    func fetchObjectsWithOffset<T: Object>(_ type: T.Type, _ predicate: NSPredicate? = nil, _ order: [SortDescriptor]? = nil, offset: Int, limit: Int ) -> [T] {
        var results = DBManager.sharedInstance.database.objects(type)
        if predicate != nil {
            results = results.filter(predicate!)
        }
        if order != nil {
            results = results.sorted(by: order!)
        }
        
        return results.get(offset: offset, limit: limit)
    }
    
    func addObject(object: Object?, update: Realm.UpdatePolicy = .modified) {
        safeTransaction {
            if object != nil {
                DBManager.sharedInstance.database.add(object!, update: update)
            }
        }
    }
    
    
    func editObjects(object: Object?) {
        safeTransaction {
            if object != nil {
                DBManager.sharedInstance.database.add(object!, update: .all)
            }
        }
    }
    
    func editObject(object:Object,key:String,value:Any?){
        safeTransaction {
            object[key] = value
            DBManager.sharedInstance.database.add(object, update: .modified)
        }
    }
    
    func addAllObjects<T: Object>(list: [T]?, update: Realm.UpdatePolicy = .modified) {
        safeTransaction {
            DBManager.sharedInstance.database.add(list!, update: update)
        }
    }
    
    func addAllObjectsFormDic<T: Object>(_ type: T.Type, value: Any = [:], update: Realm.UpdatePolicy = .modified){
        safeTransaction {
            DBManager.sharedInstance.database.create(type, value: value, update: update)
        }
    }
    
    func updateObject(updateBlock: @escaping () -> ()) {
        safeTransaction {
            updateBlock()
        }
    }
    
    
    func deleteObject(_ object: Object?) {
        safeTransaction {
            if object != nil {
                DBManager.sharedInstance.database.delete(object!)
            }
        }
    }
    
    func deleteObjects<T: Object>(_ type: T.Type, _ predicate: NSPredicate? = nil) {
        safeTransaction {
            var results = DBManager.sharedInstance.database.objects(type)
            if predicate != nil {
                results = results.filter(predicate!)
            }
            DBManager.sharedInstance.database.delete(results)
        }
    }
    
    func deleteAllFromDatabase()  {
        safeTransaction {
            DBManager.sharedInstance.database.deleteAll()
        }
    }
    /*
     func incrementID<T: Object>(_ type: T.Type, _ key :  String = "id") -> Int {
     let realm = try! Realm()
     return (realm.objects(type.self).max(ofProperty: key) as Int? ?? 0) + 1
     }
     */
    func incrementID<T: Object>(_ type: T.Type, _ key :  String = "id", _ predicate: NSPredicate? = nil) -> Int {
        var results = DBManager.sharedInstance.database.objects(type)
        if predicate != nil {
            results = results.filter(predicate!)
        }
        return (results.max(ofProperty: key) as Int? ?? 0) + 1
    }
    
}


extension Results {
    func toArray<T>(ofType: T.Type) -> [T] {
        var array = [T]()
        for i in 0 ..< count {
            if let result = self[i] as? T {
                array.append(result)
            }
        }
        
        return array
    }
    
    func get<T> (offset: Int, limit: Int ) -> [T]{
        //create variables
        var lim = 0 // how much to take
        var off = 0 // start from
        var array = [T]()
        
        //check indexes
        if offset<=self.count {
            off = offset
        }
        if limit > self.count {
            lim = self.count
        }else{
            lim = limit + off
        }
        if lim > self.count{
            lim = self.count
        }
        
        //do slicing
        for i in off..<lim{
            let obj = self[i] as! T
            array.append(obj)
        }
        
        //results
        return array
    }
    
}
